#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Ralf Ulrich, Di 20. Jul 21:44:13 CEST 2021
License: GPLv3

Load images and draw axes (log/lin) on top. Digitize graphs and curves. 
Save and edit your work later if needed.

YAML output, that is easy to read in python and plot with matplotlib.
"""

version = "1.1.4"

import sys, os, math

import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
    
from enum import Enum

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class AxisType(Enum):
     '''Type of axis
     '''
     LINEAR = 1
     LOG10 = 2
     LN = 3
axisTypes = { "linear" : AxisType.LINEAR,
              "log10" : AxisType.LOG10,
              "ln" : AxisType.LN }

class ErrorType(Enum):
     '''Type of uncertainties
     '''
     NONE = 1
     SYMMETRIC = 2
     ASYMMETRIC = 3
errorTypes = { "none" : ErrorType.NONE,
               "symmetric" : ErrorType.SYMMETRIC,
               "asymmetric" : ErrorType.ASYMMETRIC }


class Data():
     '''Storage class for the data points and uncertanties of one graph. 
     This class not only holds the data itself, but also the control elements in the GUI for the graph. 
     '''
     def __init__(self, Main, Index, xErrorType=ErrorType.NONE, yErrorType=ErrorType.NONE, name=None):
          self.Main = Main
          self.Index = Index
          self.color = QColor(QColor.colorNames()[self.Index+10])
          self.colorLabel = QLabel()
          self.points = [] # for data  points
          self.errX = xErrorType
          self.errY = yErrorType
          if not name:
               name = "GraphData_{}".format(self.Index)
          self.graphName = QLineEdit(name)

     def getName(self):
          return self.graphName.text()
          
     def addControls(self, activeIndex):
          layoutD = QHBoxLayout()
          # name:
          font = self.graphName.font()
          bgCol = "255,255,255"
          if activeIndex == self.Index:
               font.setWeight(QFont.Black)
               bgCol = "235,250,255"
          else:
               font.setWeight(QFont.Normal)
          self.graphName.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));
          self.graphName.setFont(font)
          self.graphName.focusInEvent = self.setFocus          
          layoutD.addWidget(self.graphName)
          # errors
          errStr = { b:a for a,b in errorTypes.items() }
          layoutD.addWidget(QLabel("Uncertainties: x={}, y={}".format(errStr[self.errX], errStr[self.errY])))
          # color choice
          layoutD.addWidget(QLabel("Color: "))
          self.colorLabel.setFixedWidth(60)
          self.colorLabel.mousePressEvent = self.colorPicker
          self.colorLabel.setAutoFillBackground(True)
          rgb = "{r},{g},{b}".format(r=self.color.red(),
                                     g=self.color.green(),
                                     b=self.color.blue())
          self.colorLabel.setStyleSheet("QLabel {{ background-color: rgb({}); border: 3px; }}".format(rgb))
          layoutD.addWidget(self.colorLabel)
          # remove:
          buttonRemove = QPushButton("Remove")
          buttonRemove.clicked.connect(self.removeData)
          layoutD.addWidget(buttonRemove)
          return layoutD

     def setFocus(self, evt):
          self.Main.setFocus(self.Index)
     
     def removeData(self):
          self.Main.removeData(self.Index)
     
     def colorPicker(self, evt):
          self.color = QColorDialog.getColor()
          alpha  = 140
          rgb = "{r},{g},{b}".format(r=self.color.red(),
                                     g=self.color.green(),
                                     b=self.color.blue())
          self.colorLabel.setStyleSheet("QLabel {{ background-color: rgb({}); border: 3px; }}".format(rgb))

          
class DataArea(QLabel):
     '''   Main  area to hold input image and overlay data
     '''
     def __init__(self):
          self.image = None
          self.editPoint = (-1,-1)
          self.points = [] # for axis data
          self.mousePos = None # current mouse position
          self.data = {} # graph data
          self.nextIndex = 0
          self.activeIndex = -3 # -3 means: nothing selected
          super().__init__()
          self.setAutoFillBackground(True)

          # incomplete data point, still need to get error-bar input
          self.needErrors = False


     def toAxis(self, P):
          '''Use unit vectors along user axes to calculate in axis-frame
          '''
          x,y = P.x(), P.y()
          eX_x = self.points[1]["x"] - self.points[0]["x"]
          eX_y = self.points[0]["y"] - self.points[1]["y"]
          eX_n = math.sqrt(math.pow(eX_x,2) + math.pow(eX_y,2))
          eX_x /= eX_n
          eX_y /= eX_n
          eY_x = self.points[3]["x"] - self.points[2]["x"]
          eY_y = self.points[2]["y"] - self.points[3]["y"]
          eY_n = math.sqrt(math.pow(eY_x,2) + math.pow(eY_y,2))
          eY_x /= eY_n
          eY_y /= eY_n
          print (eX_x, eX_y, eY_x, eY_y)
          return QPoint(x*eX_x+y*eX_y, -x*eY_x+y*eY_y)
          
     def fromAxis(self, P):
          '''From (axis) unit vectors to image-frame
          '''
          x,y = P.x(), P.y()
          eX_x = self.points[1]["x"] - self.points[0]["x"]
          eX_y = self.points[0]["y"] - self.points[1]["y"]
          eX_n = math.sqrt(math.pow(eX_x,2) + math.pow(eX_y,2))
          eX_x /= eX_n
          eX_y /= eX_n
          eY_x = self.points[3]["x"] - self.points[2]["x"]
          eY_y = self.points[2]["y"] - self.points[3]["y"]
          eY_n = math.sqrt(math.pow(eY_x,2) + math.pow(eY_y,2))
          eY_x /= eY_n
          eY_y /= eY_n
          print (eX_x, eX_y, eY_x, eY_y)
          return QPoint(x*eX_x+y*eY_x,  x*eX_y+y*eY_y)
          
     def minimumSizeHint(self): return QSize(800, 600)
     def sizeHint(self): return QSize(800, 600)
      
     def paintEvent(self, event):
          '''All the visual elements of the Data/Graphs
          '''
          if self.image == None:
               return
          qp = QPainter()
          qp.begin(self)
          qp.setRenderHints(QPainter.SmoothPixmapTransform, 1) # QPainter.Antialiasing | 
          qp.drawPixmap(self.rect(), self.image)
          W = self.rect().width()
          H = self.rect().height()
          color = Qt.red

          # draw axes
          if len(self.points) >= 1 :
               X1,Y1 = self.points[0]["x"], self.points[0]["y"]
               if len(self.points) >= 2 :
                    X2,Y2 = self.points[1]["x"], self.points[1]["y"]
               else:
                    X2,Y2 = self.mousePos.x()/W, self.mousePos.y()/H
               X1 = int(X1*W)
               X2 = int(X2*W)
               Y1 = int(Y1*H)
               Y2 = int(Y2*H)
               if (self.activeIndex == -1): # axis selected
                    qp.setBrush(QBrush(color, Qt.NoBrush))
                    qp.setPen(QPen(color, 6))
                    radius = 10
                    if (self.editPoint[1] == 0):
                         qp.drawEllipse(int(X1-radius), int(Y1-radius),
                                        2*radius, 2*radius)
                    elif (self.editPoint[1] == 1):
                         qp.drawEllipse(int(X2-radius), int(Y2-radius),
                                        2*radius, 2*radius)
               else:
                    qp.setPen(QPen(color, 4))
               qp.drawLine(X1, Y1, X2, Y2)
               qp.setBrush(QBrush(color, Qt.SolidPattern))
               qp.drawPolygon(QPolygon([QPoint(X2,Y2), QPoint(X2-20,Y2+10), QPoint(X2-20,Y2-10)]))
               
          if len(self.points) >= 3 :
               X1,Y1 = self.points[2]["x"], self.points[2]["y"]
               if len(self.points) >= 4 :
                    X2,Y2 = self.points[3]["x"], self.points[3]["y"]
               else:
                    X2,Y2 = self.mousePos.x()/W, self.mousePos.y()/H
               X1 = int(X1*W)
               X2 = int(X2*W)
               Y1 = int(Y1*H)
               Y2 = int(Y2*H)
               if (self.activeIndex == -2): # axis selected
                    qp.setBrush(QBrush(color, Qt.NoBrush))
                    qp.setPen(QPen(color, 6))
                    radius = 10
                    if (self.editPoint[1] == 2):
                         qp.drawEllipse(int(X1-radius), int(Y1-radius),
                                        2*radius, 2*radius)
                    elif (self.editPoint[1] == 3):
                         qp.drawEllipse(int(X2-radius), int(Y2-radius),
                                        2*radius, 2*radius)
               else:
                    qp.setPen(QPen(color, 4))
               qp.drawLine(X1, Y1, X2, Y2)
               qp.setBrush(QBrush(color, Qt.SolidPattern))
               #onAx2 = self.toAxis(QPoint(X2,Y2))
               #self.fromAxis(onAx2)
               qp.drawPolygon(QPolygon([QPoint(X2,Y2), QPoint(X2-10,Y2+20), QPoint(X2+10,Y2+20)]))
               
          # draw data
          for DIndex,D in self.data.items():
               qp.setBrush(QBrush(color, Qt.NoBrush))
               for I,P in enumerate(D.points):
                    # plot Central Marker
                    X,Y = P["x"], P["y"]
                    if self.editPoint == (DIndex, I) and not self.needErrors:
                         qp.setPen(QPen(D.color, 6))
                    else:
                         qp.setPen(QPen(D.color, 4))                       
                    radius = 5
                    qp.drawEllipse(int(X*W-radius), int(Y*H-radius),
                                   2*radius, 2*radius)
                    # plot uncertainties
                    if D.errY == ErrorType.SYMMETRIC:
                         if "errY" in P:
                              errY = P["errY"]
                         else:
                              errY = abs(self.mousePos.y()/H - Y)
                         # above marker
                         qp.drawLine(int(X*W), int(Y*H+radius), int(X*W), int((Y+errY)*H))
                         # "tick"
                         qp.drawLine(int(X*W-radius), int((Y+errY)*H), int(X*W+radius), int((Y+errY)*H))
                         # below marker
                         qp.drawLine(int(X*W), int(Y*H-radius), int(X*W), int((Y-errY)*H))
                         # "tick"
                         qp.drawLine(int(X*W-radius), int((Y-errY)*H), int(X*W+radius), int((Y-errY)*H))
                    if D.errY == ErrorType.ASYMMETRIC:
                         if "errYmin" in P:
                              errYmin = P["errYmin"]
                              if "errYmax" in P:
                                   errYmax = P["errYmax"]
                              else:
                                   errYmax = max(0, Y - self.mousePos.y()/H)
                         else:
                              errYmin = max(0, self.mousePos.y()/H - Y)
                              errYmax = None
                         if errYmax:
                              # above marker
                              qp.drawLine(int(X*W), int(Y*H-radius), int(X*W), int((Y-errYmax)*H))
                              # "tick"
                              qp.drawLine(int(X*W-radius), int((Y-errYmax)*H), int(X*W+radius), int((Y-errYmax)*H))
                         # below marker
                         qp.drawLine(int(X*W), int(Y*H+radius), int(X*W), int((Y+errYmin)*H))
                         # "tick"
                         qp.drawLine(int(X*W-radius), int((Y+errYmin)*H), int(X*W+radius), int((Y+errYmin)*H))
                  
          qp.end()



class AxisInput():
    '''
    '''
    def __init__(self, parent, layoutTo):
        self.Type = AxisType.LINEAR

        self.CB = QComboBox()
        self.CB.addItems(axisTypes.keys())
        self.CB.model().item(2).setEnabled(False)
        self.CB.currentIndexChanged.connect(self.getType)
        layoutTo.addWidget(self.CB)
        
        layoutTo.addWidget(QLabel("From:"))
        self.Min = QLineEdit()
        self.Min.setValidator(QDoubleValidator())
        layoutTo.addWidget(self.Min)
        
        layoutTo.addWidget(QLabel("To:"))
        self.Max = QLineEdit()
        self.Max.setValidator(QDoubleValidator())
        layoutTo.addWidget(self.Max)
        
        layoutTo.addWidget(QLabel("Title:"))
        self.Title = QLineEdit()
        layoutTo.addWidget(self.Title)
        
        layoutTo.addWidget(QLabel("Unit:"))
        self.Unit = QLineEdit()
        layoutTo.addWidget(self.Unit)

        self.Min.installEventFilter(parent)
        self.Max.installEventFilter(parent)
        self.Title.installEventFilter(parent)
        self.Unit.installEventFilter(parent)
        
    def getType(self, i):
        self.Type = axisTypes[self.CB.currentText()]

    def setBgColor(self, bgCol):
        self.Min.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));
        self.Max.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));
        self.Title.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));
        self.Unit.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));

      
class MainWindow(QMainWindow):
     '''
     '''     
     def __init__(self, fileName=None):
         
          super().__init__()

          self.scaleFactor = 0.0
          
          self.resize(810, 300)
          self.setWindowTitle('Data Grabber')
          
          layout = QVBoxLayout()
          layout1 = QHBoxLayout()
          layout2 = QHBoxLayout()
          self.layout3 = QVBoxLayout()
          
          # for error selection
          self.errorSelectX = QComboBox()
          self.errorSelectX.addItems(errorTypes.keys())
          self.errorSelectY = QComboBox()
          self.errorSelectY.addItems(errorTypes.keys())
          
          self.area = DataArea()
          self.area.setBackgroundRole(QPalette.Base)
          self.area.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
          self.area.setGeometry(0, 0, 800, 600)
          self.area.setScaledContents(True)
          self.area.setMouseTracking(True)
          
          self.scrollArea = QScrollArea()
          self.scrollArea.setBackgroundRole(QPalette.Dark)
          self.scrollArea.setWidget(self.area)
          self.scrollArea.setVisible(False)

          # x axis input fields
          layout1.addWidget(QLabel("X-Axis:"))
          self.xAxisInput = AxisInput(self, layout1)
          
          # y axis input fields
          layout2.addWidget(QLabel("Y-Axis:"))
          self.yAxisInput = AxisInput(self, layout2)

          # new graph data input fields
          self.layout3.addLayout(self.addNewDataControl())

          # final gui setup
          layout.addWidget(self.scrollArea)
          layout.addLayout(layout1)
          layout.addLayout(layout2)
          layout.addLayout(self.layout3)

          widget = QWidget()
          widget.setLayout(layout)
          self.setCentralWidget(widget)
          
          self.setMouseTracking(True)

          self.createActions()
          self.createMenus()

          self.area.installEventFilter(self)
          self.scrollArea.viewport().installEventFilter(self)
          
          self.fileName = fileName
          self.loadFile(self.fileName)

          # not in move mode
          self.moveMode = False

     def convertLinearX(self, x):
          '''Convert screen position x to graph/axis position
          '''
          axMin = self.area.points[0]["x"]
          axMax = self.area.points[1]["x"]
          return self.xMin + (x-axMin) * (self.xMax - self.xMin) / (axMax - axMin)

     def convertLinearY(self, y):
          '''Convert screen position y to graph/axis position
          '''
          axMin = self.area.points[2]["y"]
          axMax = self.area.points[3]["y"]
          return self.yMin + (y-axMin) * (self.yMax - self.yMin) / (axMax - axMin)

     def convertLogX(self, x):
          '''Convert screen position x to graph/axis position for log10
          '''
          axMin = self.area.points[0]["x"]
          axMax = self.area.points[1]["x"]
          return math.pow(10., math.log10(self.xMin) + (x-axMin) * (math.log10(self.xMax) - math.log10(self.xMin)) / (axMax - axMin))

     def convertLogY(self, y):
          '''Convert screen position x to graph/axis position for log10
          '''
          axMin = self.area.points[2]["y"]
          axMax = self.area.points[3]["y"]
          return math.pow(10., math.log10(self.yMin) + (y-axMin) * (math.log10(self.yMax) - math.log10(self.yMin)) / (axMax - axMin))

     def convertFromLinearX(self, x):
          '''Convert graph/axis x position to screen position x
          '''
          axMin = self.area.points[0]["x"]
          axMax = self.area.points[1]["x"]
          return axMin + (x-self.xMin) * (axMax - axMin) / (self.xMax - self.xMin)

     def convertFromLinearY(self, y):
          '''Convert graph/axis position to screen position y
          '''
          axMin = self.area.points[2]["y"]
          axMax = self.area.points[3]["y"]
          return axMin + (y-self.yMin) * (axMax - axMin) / (self.yMax - self.yMin)

     def convertFromLogX(self, x):
          '''Convert graph/axis position for log10 to screen position x
          '''
          axMin = self.area.points[0]["x"]
          axMax = self.area.points[1]["x"]
          return axMin + (math.log10(x)-math.log10(self.xMin)) * (axMax - axMin) / (math.log10(self.xMax) - math.log10(self.xMin))

     def convertFromLogY(self, y):
          '''Convert graph/axis position for log10 to screen position y
          '''
          axMin = self.area.points[2]["y"]
          axMax = self.area.points[3]["y"]
          return axMin + (math.log10(y)-math.log10(self.yMin)) * (axMax - axMin) / (math.log10(self.yMax) - math.log10(self.yMin))
     
     
     def setFocus(self, Index):
          bgColNormal = "255,255,255"
          bgColHigh = "235,250,255"
          self.area.activeIndex = Index
          for DIndex,D in self.area.data.items():
               font = D.graphName.font()
               bgCol = bgColNormal
               if self.area.activeIndex == DIndex:
                    font.setWeight(QFont.Black)
                    bgCol = bgColHigh
               else:
                    font.setWeight(QFont.Normal)
               D.graphName.setFont(font)
               D.graphName.setStyleSheet("QLineEdit {{ background: rgb({bg}); }}".format(bg=bgCol));
          # x axis
          bgCol = bgColNormal
          if Index == -1:
               bgCol = bgColHigh
          self.xAxisInput.setBgColor(bgCol)
          # y axis
          bgCol = bgColNormal
          if Index == -2:
               bgCol = bgColHigh
          self.yAxisInput.setBgColor(bgCol)

     def removeData(self, Index):
          self.clearDataLayout()

          # delete data
          del self.area.data[Index]
          
          # re-create layout
          for D in self.area.data.values():
               self.layout3.addLayout(D.addControls(self.area.activeIndex))
          # done
          self.layout3.addLayout(self.addNewDataControl())
          self.area.update()
               
     def eventFilter(self, obj, evt):
          '''Function to handle most of the interactive elements of the GUI
          '''

          # first check, wether currently an incomplete point exists
          self.area.needErrors = False
          if (self.area.activeIndex in self.area.data.keys()):
               if (self.area.data[self.area.activeIndex].errY == ErrorType.SYMMETRIC and
                   len(self.area.data[self.area.activeIndex].points) > 0 and
                   not "errY" in self.area.data[self.area.activeIndex].points[-1]):
                    self.area.needErrors = True
               if (self.area.data[self.area.activeIndex].errY == ErrorType.ASYMMETRIC and
                   len(self.area.data[self.area.activeIndex].points) > 0 and
                   (not "errYmin" in self.area.data[self.area.activeIndex].points[-1] or
                    not "errYmax" in self.area.data[self.area.activeIndex].points[-1])):
                    self.area.needErrors = True
                    
          # mouse events in main scroll-area (of DataArea) 
          if obj is self.scrollArea.viewport():

               if evt.type() == QEvent.Wheel:
                    if self.area.editPoint[0] != -1: # ignore axis points
                         
                         # update old uncertainties

                         increase = 1
                         if evt.angleDelta().y() < 0:
                              increase = -1
                         
                         modi = evt.modifiers()
                         if modi == Qt.ControlModifier:
                              if "errY" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errY"] *= 1+increase*0.01
                              if "errYmin" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errYmin"] *= 1+increase*0.01
                         elif modi == (Qt.ControlModifier | Qt.ShiftModifier):
                              if "errYmax" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errYmax"] *= 1+increase*0.01
                         elif modi == Qt.AltModifier:
                              if "errX" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errX"] *= 1+increase*0.01
                              if "errXmin" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errXmin"] *= 1+increase*0.01
                         elif modi == (Qt.AltModifier | Qt.ShiftModifier):
                              if "errXmax" in self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]:
                                   self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["errXmax"] *= 1+increase*0.01
                         
                         self.area.update()
                         return True

          # axis config?
          elif (obj is self.xAxisInput.Min or obj is self.xAxisInput.Max or
                obj is self.xAxisInput.Title or obj is self.xAxisInput.Unit):
               if evt.type() == QEvent.FocusIn:
                    self.setFocus(-1)
                    self.area.update()

          elif (obj is self.yAxisInput.Min or obj is self.yAxisInput.Max or
                obj is self.yAxisInput.Title or obj is self.yAxisInput.Unit):
               if evt.type() == QEvent.FocusIn:
                    self.setFocus(-2)
                    self.area.update()
                    
          # mouse events in main DataArea
          elif obj is self.area:

               # new data point, or modify point
               if evt.type() == QEvent.MouseButtonPress:
                    if evt.buttons() == Qt.LeftButton:
                         X = evt.x()/self.area.rect().width()
                         Y = evt.y()/self.area.rect().height()
                         # new point, no editPoint
                         if self.area.editPoint == (-1,-1) or self.area.needErrors:
                              if len(self.area.points) < 4:
                                   # this is still the axes
                                   self.area.points.append( {"x":X, "y":Y} )
                              else:
                                   # this is data points
                                   # thus, we need an active Graph !
                                   if (not self.area.activeIndex in self.area.data.keys()):
                                        msgBox = QErrorMessage()
                                        msgBox.showMessage("You first must create/select a graph")
                                        msgBox.exec_()
                                        return True
                               
                                   if (self.area.data[self.area.activeIndex].errY == ErrorType.SYMMETRIC and
                                       self.area.needErrors):
                                        # symmetric error is not yet recorded -> do now
                                        P = self.area.data[self.area.activeIndex].points[-1]
                                        self.area.data[self.area.activeIndex].points[-1]["errY"] = abs(Y-P["y"])

                                   elif (self.area.data[self.area.activeIndex].errY == ErrorType.ASYMMETRIC and
                                         self.area.needErrors):
                                        # asymmetric error is not yet recorded -> do now. First ymin, then ymax
                                        P = self.area.data[self.area.activeIndex].points[-1]
                                        if not "errYmin" in self.area.data[self.area.activeIndex].points[-1]:
                                             self.area.data[self.area.activeIndex].points[-1]["errYmin"] = max(0, Y - P["y"])
                                        else:
                                             self.area.data[self.area.activeIndex].points[-1]["errYmax"] = max(0, P["y"] - Y)

                                   else:
                                        # previous errors already recorded -> new data point
                                        self.area.data[self.area.activeIndex].points.append( {"x":X, "y":Y} )
                                        self.moveMode = False
                                        
                         else: # update existing point
                              self.moveMode = True
           
                    elif evt.buttons() == Qt.RightButton:
                         # delete active data point
                         if self.area.editPoint[0] != -1:
                              del self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]
                              self.area.editPoint = (-1,-1) # reset                

                    self.area.update()
                    return True

               # release mouse button (left)
               elif evt.type() == QEvent.MouseButtonRelease:
                    self.moveMode = False
                    return True
 
               # mouse moved --> check closeby objects
               #                 but only use "active" objects (graph/axis)
               elif evt.type() == QEvent.MouseMove:
                    if self.moveMode:
                         X = evt.x()/self.area.rect().width()
                         Y = evt.y()/self.area.rect().height()                         
                         if self.area.editPoint[0] == -1:
                              # update old axis point
                              self.area.points[self.area.editPoint[1]] = {"x":X, "y":Y}
                         else:
                              # update old data point
                              self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["x"] = X
                              self.area.data[self.area.editPoint[0]].points[self.area.editPoint[1]]["y"] = Y
                         self.area.update()
                         return True
     
                    else:
                         # search for new close point
                         self.area.editPoint = (-1,-1)
                         M = evt.pos()
                         Mx = M.x() / self.area.rect().width()
                         My = M.y() / self.area.rect().height()
                         minD = 1e10
                         minI = (-1,-1)
                         # modify axis points, only if all axes have been drawn completely !
                         if (len(self.area.points) >= 4):
                              for I, pnt in enumerate(self.area.points):
                                   X,Y = pnt["x"], pnt["y"]
                                   dist = math.sqrt(math.pow(X-Mx,2) + math.pow(Y-My,2))
                                   # axis X is active
                                   if (self.area.activeIndex==-1 and (I==0 or I==1)): 
                                        if dist<minD:
                                             minD = dist
                                             minI = (-1,I)
                                   # axis Y is active
                                   if (self.area.activeIndex==-2 and (I==2 or I==3)): 
                                        if dist<minD:
                                             minD = dist
                                             minI = (-1,I)
                         # move data points
                         if self.area.activeIndex in self.area.data:
                              for I, pnt in enumerate(self.area.data[self.area.activeIndex].points):
                                   X,Y = pnt["x"], pnt["y"]
                                   dist = math.sqrt(math.pow(X-Mx,2) + math.pow(Y-My,2))
                                   if dist<minD:
                                        minD = dist
                                        minI = (self.area.activeIndex,I)
                         if minD<.03:
                              self.area.editPoint = minI
                         self.area.mousePos = M
                    self.area.update()
                     
          return super().eventFilter(obj, evt)        

     def addNewDataControl(self):
          layoutD = QHBoxLayout()
          # uncertainties config
          layoutD.addWidget(QLabel("Uncertainties x="))
          layoutD.addWidget(self.errorSelectX)
          self.errorSelectX.setEnabled(False)
          layoutD.addWidget(QLabel("Uncertainties y="))
          layoutD.addWidget(self.errorSelectY)
          # button
          buttonNewData = QPushButton("New Graph")
          buttonNewData.clicked.connect(self.newData)
          layoutD.addWidget(buttonNewData)
          return layoutD

     def clearDataLayout(self):
          # clean layout
          for i in reversed(range(self.layout3.count())):
               L = self.layout3.itemAt(i).layout()
               if (L == None): print (" ERROR ")
               for j in reversed(range(L.count())):
                    w = L.itemAt(j).widget()
                    if w == None: print (" ERROR2 ")
                    w.setParent(None)


    
     def newData(self):
          if not self.area.image:
               msgBox = QErrorMessage()
               msgBox.showMessage("No image loaded. Load or Paste before creating any graph.")
               msgBox.exec_()
               return

          self.area.nextIndex += 1
          self.area.activeIndex = self.area.nextIndex
          self.area.data[self.area.nextIndex] = Data(self,
                                                     self.area.nextIndex,
                                                     ErrorType.NONE,
                                                     errorTypes[self.errorSelectY.currentText()])
          self.clearDataLayout()
          # re-create layout
          for D in self.area.data.values():
               self.layout3.addLayout(D.addControls(self.area.activeIndex))
          self.layout3.addLayout(self.addNewDataControl())

     def saveData(self):
          '''Save all data and information to a file. Including the pixmap data. 
          '''          
          def is_number(string):
               try:
                    float(string)
                    return True
               except ValueError:
                    return False

          if (len(self.area.points) != 4):
               msgBox = QErrorMessage()
               msgBox.showMessage("Missing axis. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return
         
          # check axis data
          if (not is_number(self.xAxisInput.Min.text()) or
              not is_number(self.xAxisInput.Max.text()) or
              not is_number(self.yAxisInput.Min.text()) or
              not is_number(self.yAxisInput.Max.text())):
               msgBox = QErrorMessage()
               msgBox.showMessage("Missing axis ranges. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return

          self.xMin = float(self.xAxisInput.Min.text())
          self.xMax = float(self.xAxisInput.Max.text())
          self.yMin = float(self.yAxisInput.Min.text())
          self.yMax = float(self.yAxisInput.Max.text())
          
          if (self.xMin == self.xMax or self.yMin == self.yMax):
               msgBox = QErrorMessage()
               msgBox.showMessage("Axis range is zero. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return              
         
          if (self.xAxisInput.Type != AxisType.LINEAR and # thus logarithmic
              (self.xMin <= 0 or
               self.xMax <= 0)):
               msgBox = QErrorMessage()
               msgBox.showMessage("Negative boundary(ies) for log x-axis. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return

          if (self.yAxisInput.Type != AxisType.LINEAR and # thus logarithmic
              (self.yMin <= 0 or
               self.yMax <= 0)):
               msgBox = QErrorMessage()
               msgBox.showMessage("Negative boundary(ies) for log y-axis. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return

          if self.xAxisInput.Type == AxisType.LINEAR:
               convertX = self.convertLinearX
          elif self.xAxisInput.Type == AxisType.LOG10:
               convertX = self.convertLogX
          else:
               msgBox = QErrorMessage()
               msgBox.showMessage("Unsupported X Axis type. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return
          if self.yAxisInput.Type == AxisType.LINEAR:
               convertY = self.convertLinearY
          elif self.yAxisInput.Type == AxisType.LOG10:
               convertY = self.convertLogY
          else:
               msgBox = QErrorMessage()
               msgBox.showMessage("Unsupported Y Axis type. CANNOT SAVE. FIX!")
               msgBox.exec_()
               return
               
          fileName, _ = QFileDialog.getSaveFileName(self, 'Save Graphs', '',
                                                    'Data (*.yaml)',
                                                    options=QFileDialog.DontUseNativeDialog)
          fileNameBase,fileNameExtension = os.path.splitext(fileName)
          if fileNameExtension != ".yaml":
              fileName = fileName + ".yaml"
          fileNamePNG = fileNameBase + "_data.png"

          axStr = { b:a for a,b in axisTypes.items() } # invert
          errStr = { b:a for a,b in errorTypes.items() } # invert
          # first save pixmap data
          try:
               self.area.image.save(fileNamePNG, "PNG")
          except IOError as error:
               QMessageBox.information(self, "Data Grabber", "WARNING: Plot data as PNG not saved.")
               
          try:
               with open(fileName, "w") as stream:
                    saveData = { "input": fileNamePNG,
                                 "original_input": self.fileName,
                                 "x-axis" : { "type" : axStr[self.xAxisInput.Type],
                                              "title" : self.xAxisInput.Title.text(),
                                              "unit" : self.xAxisInput.Unit.text(),
                                              "min" : self.xAxisInput.Min.text(),
                                              "max" : self.xAxisInput.Max.text(),
                                              "p1" : {"x":self.area.points[0]["x"],"y":self.area.points[0]["y"]},
                                              "p2" : {"x":self.area.points[1]["x"],"y":self.area.points[1]["y"]}},
                                 "y-axis" : { "type" : axStr[self.yAxisInput.Type],
                                              "title" : self.yAxisInput.Title.text(),
                                              "unit" : self.yAxisInput.Unit.text(),
                                              "min" : self.yAxisInput.Min.text(),
                                              "max" : self.yAxisInput.Max.text(),
                                              "p1" : {"x":self.area.points[2]["x"],"y":self.area.points[2]["y"]},
                                              "p2" : {"x":self.area.points[3]["x"],"y":self.area.points[3]["y"]}}}
                    saveData["graphs"] = dict()
                    for D in self.area.data.values():
                         name = D.getName()
                         saveData["graphs"][name] = dict()
                         saveData["graphs"][name]["cfg"] = { "errX":errStr[D.errX], "errY":errStr[D.errY] }
                         saveData["graphs"][name]["data"] = dict()
                         saveData["graphs"][name]["data"]["x"] = list()
                         saveData["graphs"][name]["data"]["y"] = list()              
                         saveData["graphs"][name]["data"]["x"] = [ convertX(v["x"]) for v in D.points ]
                         saveData["graphs"][name]["data"]["y"] = [ convertY(v["y"]) for v in D.points ]
                         if D.errY == ErrorType.SYMMETRIC:
                              saveData["graphs"][name]["data"]["errY"] = list()
                              saveData["graphs"][name]["data"]["errY"] = [ abs(convertY(v["y"]-v["errY"]) - convertY(v["y"])) for v in D.points ]
                         if D.errY == ErrorType.ASYMMETRIC:
                              saveData["graphs"][name]["data"]["errYmin"] = list()              
                              saveData["graphs"][name]["data"]["errYmin"] = [ convertY(v["y"])-convertY(v["y"]+v["errYmin"]) for v in D.points ]
                              saveData["graphs"][name]["data"]["errYmax"] = list()              
                              saveData["graphs"][name]["data"]["errYmax"] = [ convertY(v["y"]-v["errYmax"])-convertY(v["y"]) for v in D.points ]
                    yaml.dump(saveData, stream)
                    stream.close()
          except IOError as error:
               QMessageBox.information(self, "Data Grabber", "WARNING: Data not saved.")
               
         
     def zoomIn(self):
          self.scaleImage(1.25)

     def pasteImage(self):
          clipboard = QApplication.clipboard()
          image = clipboard.pixmap()
          if image.isNull():
               QMessageBox.information(self, "Data Grabber", "Cannot paste from clipboard.")
               return
          self.area.image = image.scaledToWidth(self.area.width(), Qt.SmoothTransformation);
          self.scaleFactor = 1.0

          self.scrollArea.setVisible(True)
          self.fitToWindowAct.setEnabled(True)
          self.updateActions()            
          
          if not self.fitToWindowAct.isChecked():
               self.area.adjustSize()

          self.area.update()
          
     def openFile(self):          
          fileName, _ = QFileDialog.getOpenFileName(self, 'QFileDialog.getOpenFileName()', '',
                                                    'Images (*.png *.jpeg *.jpg *.bmp *.gif *.yaml)',
                                                    options=QFileDialog.DontUseNativeDialog)

          self.loadFile(fileName)


     def loadYAML(self, fileName=None):
          if not fileName:
               return
          _, file_extension = os.path.splitext(fileName)
          if file_extension != ".yaml":
               return
          
          cfg_file = open(fileName, "r")
          cfg = yaml.load(cfg_file, Loader=Loader)
          cfg_file.close()
          fileName = cfg["input"]
          self.xAxisInput.Type = axisTypes[cfg["x-axis"]["type"]]
          self.xAxisInput.Title.setText(cfg["x-axis"]["title"])
          self.xAxisInput.Unit.setText(cfg["x-axis"]["unit"])
          self.xAxisInput.Min.setText(cfg["x-axis"]["min"])
          self.xAxisInput.Max.setText(cfg["x-axis"]["max"])
          self.area.points.clear()
          self.area.points.append( {"x": float(cfg["x-axis"]["p1"]["x"]),
                                    "y": float(cfg["x-axis"]["p1"]["y"])})
          self.area.points.append( {"x": float(cfg["x-axis"]["p2"]["x"]),
                                    "y": float(cfg["x-axis"]["p2"]["y"])})
          self.xAxisInput.Type = axisTypes[cfg["y-axis"]["type"]]
          self.yAxisInput.Title.setText(cfg["y-axis"]["title"])
          self.yAxisInput.Unit.setText(cfg["y-axis"]["unit"])
          self.yAxisInput.Min.setText(cfg["y-axis"]["min"])
          self.yAxisInput.Max.setText(cfg["y-axis"]["max"])
          self.area.points.append( {"x": float(cfg["y-axis"]["p1"]["x"]),
                                    "y": float(cfg["y-axis"]["p1"]["y"])})
          self.area.points.append( {"x": float(cfg["y-axis"]["p2"]["x"]),
                                    "y": float(cfg["y-axis"]["p2"]["y"])})

          self.xMin = float(self.xAxisInput.Min.text())
          self.xMax = float(self.xAxisInput.Max.text())
          self.yMin = float(self.yAxisInput.Min.text())
          self.yMax = float(self.yAxisInput.Max.text())

          if self.xAxisInput.Type == AxisType.LINEAR:
               convertX = self.convertFromLinearX
          elif self.xAxisInput.Type == AxisType.LOG10:
               convertX = self.convertFromLogX
          else:
               print ("Unknown error type")
          if self.yAxisInput.Type == AxisType.LINEAR:
               convertY = self.convertFromLinearY
          elif self.yAxisInput.Type == AxisType.LOG10:
               convertY = self.convertFromLogY
          else:
               print ("Unknown error type")

          self.area.nextIndex = 0
          if cfg["graphs"]:
               for graph in cfg["graphs"]:
                    self.area.nextIndex += 1
                    self.area.activeIndex = self.area.nextIndex
                    self.area.data[self.area.nextIndex] = Data(self, self.area.nextIndex,
                                                               errorTypes[cfg["graphs"][graph]["cfg"]["errX"]],
                                                               errorTypes[cfg["graphs"][graph]["cfg"]["errY"]], name=graph)
                    gData = cfg["graphs"][graph]["data"]
                    self.area.data[self.area.nextIndex].points.clear()
                    for iP in range(len(gData["x"])):
                         P = {"x" : convertX(gData["x"][iP]), "y" : convertY(gData["y"][iP]) }
                         if self.area.data[self.area.nextIndex].errY == ErrorType.SYMMETRIC:
                              P["errY"] = abs(convertY(gData["errY"][iP]+gData["y"][iP]) - convertY(gData["y"][iP]))
                         if self.area.data[self.area.nextIndex].errY == ErrorType.ASYMMETRIC:
                              P["errYmin"] = (convertY(gData["y"][iP]-gData["errYmin"][iP])) - convertY(gData["y"][iP])
                              P["errYmax"] = convertY(gData["y"][iP]) - (convertY(gData["y"][iP]+gData["errYmax"][iP]))

                         self.area.data[self.area.nextIndex].points.append(P)

          # setup GUI
          self.clearDataLayout()
          # re-create layout
          for D in self.area.data.values():
               self.layout3.addLayout(D.addControls(self.area.activeIndex))
          self.layout3.addLayout(self.addNewDataControl())

          self.loadIMAGE(fileName)

          
     def loadIMAGE(self, fileName=None):
          if not fileName:
               return

          image = QPixmap(fileName)
          if image.isNull():
               QMessageBox.information(self, "Data Grabber", "Cannot load plot %s." % fileName)
               return
          self.area.image = image.scaledToWidth(self.area.width(), Qt.SmoothTransformation);
          self.scaleFactor = 1.0

          self.scrollArea.setVisible(True)
          self.fitToWindowAct.setEnabled(True)
          self.updateActions()            
          
          if not self.fitToWindowAct.isChecked():
               self.area.adjustSize()

          # remember file location, name
          self.fileName = fileName
          
          self.area.update()
     
     def loadFile(self, fileName=None):
          if not fileName:
               return

          _, file_extension = os.path.splitext(fileName)
          if file_extension == ".yaml":
               self.loadYAML(fileName)
          else:
               self.loadIMAGE(fileName)

                        
     def zoomOut(self):
          self.scaleImage(0.8)

     def normalSize(self):
          self.area.adjustSize()
          self.scaleFactor = 1.0

     def fitToWindow(self):
          fitToWindow = self.fitToWindowAct.isChecked()
          self.scrollArea.setWidgetResizable(fitToWindow)
          if not fitToWindow:
               self.normalSize()
          self.updateActions()

     def about(self):
          QMessageBox.about(self, "Data Grabber",
                            "<p>The <b>Data Grabber</b> (v{}) grabs data from images/plots</p>"
                            "<p>Ralf Ulrich Do 15. Jul 19:43:39 CEST 2021</p>"
                            "<p>GPLv3</p>"
                            "<p>You can open files, or past a graph from the clipboard.</p>"
                            "<p>"
                            "Click to set points, first x-axis, then y-axis then graph data. "
                            "Hold left button to modify points, scroll with (Ctrl, Shift, Alt) to change uncertainties. "
                            "</p>".format(version))

     def createActions(self):
          self.exitAct = QAction("Exit", self, shortcut="Ctrl+Q", triggered=self.close)
          self.aboutAct = QAction("About", self, triggered=self.about)
          self.saveAct = QAction("Save YAML", self, shortcut="Ctrl+S", triggered=self.saveData)

          self.zoomInAct = QAction("Zoom In (25%)", self, shortcut="Ctrl++", enabled=True, triggered=self.zoomIn)
          self.zoomOutAct = QAction("Zoom Out (25%)", self, shortcut="Ctrl+-", enabled=True, triggered=self.zoomOut)
          self.normalSizeAct = QAction("Normal Size", self, shortcut="Ctrl+N", enabled=True, triggered=self.normalSize)
          self.fitToWindowAct = QAction("Fit to Window", self, enabled=False, checkable=True, shortcut="Ctrl+F",
                                        triggered=self.fitToWindow)
          
          self.openAct = QAction("Open", self, shortcut="Ctrl+O", triggered=self.openFile)
          self.pasteAct = QAction("Paste", self, shortcut="Ctrl+V", triggered=self.pasteImage)

     def createMenus(self):
          self.fileMenu = QMenu("File", self)
          self.fileMenu.addAction(self.openAct)
          self.fileMenu.addAction(self.pasteAct)
          self.fileMenu.addAction(self.saveAct)
          self.fileMenu.addSeparator()
          self.fileMenu.addAction(self.exitAct)

          self.viewMenu = QMenu("View", self)
          self.viewMenu.addAction(self.zoomInAct)
          self.viewMenu.addAction(self.zoomOutAct)
          self.viewMenu.addAction(self.normalSizeAct)
          self.viewMenu.addSeparator()
          self.viewMenu.addAction(self.fitToWindowAct)

          self.helpMenu = QMenu("Help", self)
          self.helpMenu.addAction(self.aboutAct)

          self.menuBar().addMenu(self.fileMenu)
          self.menuBar().addMenu(self.viewMenu)
          self.menuBar().addMenu(self.helpMenu)

     def updateActions(self):
          self.zoomInAct.setEnabled(not self.fitToWindowAct.isChecked())
          self.zoomOutAct.setEnabled(not self.fitToWindowAct.isChecked())
          self.normalSizeAct.setEnabled(not self.fitToWindowAct.isChecked())

     def scaleImage(self, factor):
          self.scaleFactor *= factor
          self.area.resize(self.scaleFactor * self.area.image.size())
          
          self.adjustScrollBar(self.scrollArea.horizontalScrollBar(), factor)
          self.adjustScrollBar(self.scrollArea.verticalScrollBar(), factor)
          
          self.zoomInAct.setEnabled(self.scaleFactor < 5.0)
          self.zoomOutAct.setEnabled(self.scaleFactor > 0.2)
          self.area.update()

     def adjustScrollBar(self, scrollBar, factor):
          scrollBar.setValue(int(factor * scrollBar.value()
                                 + ((factor - 1) * scrollBar.pageStep() / 2)))



        
if __name__ == '__main__':

     print ("Data Grabber ({}) by Ralf Ulrich, 2021".format(version))
     
     app = QApplication(sys.argv)
     if len(sys.argv) > 1:
          window = MainWindow(sys.argv[1])
     else:
          window = MainWindow()
     window.show()
     sys.exit(app.exec_())

